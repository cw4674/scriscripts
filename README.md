# README #

### What is this repository for? ###

Storing and sharing python scripts or other useful tools to use in conjunction with the moble scri module, available through https://github.com/moble/scri. It may be useful to follow the set up instructions found on the SXS wiki: https://www.black-holes.org/wiki/spec_postprocessing.

### How do I get set up? ###

Simply git clone this repository into your own branch to use and modify any of the files you want. If you are modifying files, please don't git push to master without requesting permission first.

###Script Descriptions###

~ COMPosition.py - Find the COM position averaged over the total time of the simulation for each resolution Lev in each simulation. This script will print a txt file with the output of all the PrecBBH000xxx/LevN names with the COM position for each. You need to include the --dir [path/to/look/for/Horizons.h5] and --filename [filename_for_output]. Note that the --dir option does not need to have Horizons.h5 in the directory explicity as the script will look through the directories down from where the --dir option stops, and the --filename option does not need extensions. Include the --name_sort flag to sort the data by name as opposed to COM position value, and the --histogram flag to include a logarithmic histogram of the data.
||||||| merged common ancestors
COMPosition.py - Find the COM position averaged over the total time of the simulation for each resolution Lev in each simulation. This script will print a txt file with the output of all the PrecBBH000xxx/LevN names with the COM position for each. You need to include the --dir [path/to/look/for/Horizons.h5] and --filename [filename_for_output]. Note that the --dir option does not need to have Horizons.h5 in the directory explicity as the script will look through the directories down from where the --dir option stops, and the --filename option does not need extensions. Include the --name_sort flag to sort the data by name as opposed to COM position value, and the --histogram flag to include a logarithmic histogram of the data.
=======
COMPosition.py - Find the COM position averaged over the total time of the simulation for each resolution Lev in each simulation. This script will print a txt file with the output of all the PrecBBH000xxx/LevN names with the COM position for each. You need to include the --dir [path/to/look/for/Horizons.h5] and --filename [filename_for_output]. Note that the --dir option does not need to have Horizons.h5 in the directory explicity as the script will look through the directories down from where the --dir option stops, and the --filename option does not need extensions. Include the --histogram flag to include a logarithmic histogram of the data.
>>>>>>> Updated scripts and took out name_sort flag

~ COMVelocity - Find the COM position averaged over the total time of the simulation for each resolution. This script works in the same way as COMPosition.py, with the same flags and usage.

~ COMPositionDrift_SpinAndMass.py - Find the COM position at the time of common horizon for each lev of each simulation as well as the mass (relaxed-mass1), spin (relaxed-spin1) from the metadata.txt, and the time averaged COM position data. This script has the same --dir and --filename required flags as the above scripts. It will output the COM position at common horizon time and the time averaged COM position against the mass and spin of each Lev of each simulation as well as a .txt file with the simulation name (PrecBBH000xxx/LevN), the time averaged COM position magnitude, the COM position at common horizon magnitude, the mass, and the spin for each available lev.

~ COMVelocityAndPositionDrift.py - Find the COM velocity averaged over all Levs in a simulation for each simulation given. The --dir and --filename required flags are the same as for the above scripts. The COM velocity data is already time averaged over the simulation. You can add the --scatter flag to add scatter plots of the components of the Lev averaged COM velocity. This will output 3 graphs in the x-y,y-z, and x-z plane. You can also add the --histogram flag to add logarithmic histogram plots of the Lev averaged COM velocity, the time averaged COM position per Lev, the COM velocity per Lev, and of the difference between the averaged COM velocity and the individual Lev COM velocities (ie. Lev deviance from their respective average).

Plot and text outputs from these scripts from the Simulation Annex can be found in the AnnexCOMPlots git repository at https://bitbucket.org/cw4674/annexcomplots/overview

~ COMCorrection.py - Compute COM motion corrected h5 directories. This script requires the --dir flag to specify where to start looking for the h5 directories. This script also has optional flags for the beginning and ending fraction of the simulation to be considered in the COM corrected data, specified by the --beginning_fraction and --ending_fraction flags. There is an optional --plot flag to that is set to False by default.

~COMCorrection_FixedBFEF.py OUTDATED!! USE COMCorrection.py instead - Same function as COMCorrection.py except BF is set as equating to the relaxation time and ef is set to 10%. This script also does not have the option to rename to the output COM files and will skip doing COM corrections if the *_CoM.h5 file already exists in the directory where *.h5 is. The only flags are --dir and --plot. This script was used to perform CoM corrections on /Catalog and /Incoming of the Simulation Annex in Jan 2017.

To obtain the scripts, paste the following into your command line:

git clone git@bitbucket.org:cw4674/scriscripts.git