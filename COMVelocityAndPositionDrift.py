#!/usr/bin/env python

"""
Calculate the COM velocity drift from any run containing 
Horizons.h5 in its path. You should speicify where to 
find the runs you want to find the COM velocity drift for, 
as well as the location and name of the output files.

The program will return nothing if the directory does not 
contain a Horizons.h5 file anywhere.

The program also has the capability of making a histogram
from the velocites found, plotting them logarithmically.

Specify "-h" to see proper usage and additional flags.
"""

import sys
import os
import argparse
import math
import numpy as np
import matplotlib.pyplot as plt

def blockPrint():
#prevent printing to screen or to files
	sys.stdout = open(os.devnull, 'w')

def enablePrint():
#enable printing
	sys.stdout = sys.__stdout__

#------------------------------------------------------------------------------------------
	
def main():
	
	#Parse arguments from command line
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument("--dir",  nargs = "+", required = True,
			    help = "Directory in which to start searching for Horizon.h5 files.")
	parser.add_argument("--filename", required = True,
			    help = "Output file name and location. This will overwrite files that already exist with the same name.")
	parser.add_argument("--scatter",default = False, action = "store_true",
			    help = "Want 2D comparison plots for the avg COM velocity per simulation")
	parser.add_argument("--histogram", default = False, action = "store_true",
			    help = "Want histograms of the avg COM velocity and velocity and position per Lev per simulation.")
	
	args = parser.parse_args()

	import scri.SpEC as SpEC

	#Find and store all of the directories within the given directory that contain Horizons.h5.
	#os.path.join combines the directory name d and file name x for simplicity when calling the 
	#scri functions.
	h5dir = [os.path.join(d,x)
		 for d, dirs, files in os.walk(args.dir[0], topdown=True)
		 for x in files if x.endswith("Horizons.h5")]

	h5dir = sorted(h5dir)
	h5dir_lev1 = []
	h5dir_lev2 = []
	h5dir_lev3 = []
	h5dir_lev4 = []
	h5dir_lev5 = []

	#Open file for writing Lev-averaged velocity values for each simulation
	f = open(args.filename+"_AvgCOMVelocity.txt","w")
	f.write("Run Name                           Average COM Velocity, (x,y,z)\n")
	f.write("\n")

	vecv_avg = []
	vecv_1 = []
	vecv_2 = []
	vecv_3 = []
	vecv_4 = []
	vecv_5 = []
	test1=test2=test3=test4=test5 = 0
	vecp_1 = []
	vecp_2 = []
	vecp_3 = []
	vecp_4 = []
	vecp_5 = []

	vdiff1 = []
	vdiff2 = []
	vdiff3 = []
	vdiff4 = []
	vdiff5 = []
	
	for in_file in h5dir:
		blockPrint() #stop estimate_avg_com_motion() from printing to screen
		
		#Store data from scri into the array cominfo. 
		#cominfo should hold items as [[x_i, x_j, x_k], [v_i, v_j, v_z], ti, tf], where
		#x and v are the COM positions and velocities.
		cominfo = SpEC.estimate_avg_com_motion(in_file)
		enablePrint()
		
		#Store velocity and position information in another array to manipulate
		v = cominfo[1]
		p = cominfo[0]

		#Assign values to appropriate Lev array. 
		#Use the "testk" counter to indicate which Lev arrays have been added to.
		if "Lev1" in in_file:
			vecv_1.append(v)
			vecp_1.append(p)
			h5dir_lev1.append(in_file[-30:-12])
			test1 = 1
		elif "Lev2" in in_file:
			vecv_2.append(v)
			vecp_2.append(p)
			test2 = 1
			h5dir_lev2.append(in_file[-30:-12])
		elif "Lev3" in in_file:
			vecv_3.append(v)
			vecp_3.append(p)
			test3 = 1
			h5dir_lev3.append(in_file[-30:-12])
		elif "Lev4" in in_file:
			vecv_4.append(v)
			vecp_4.append(p)
			test4 = 1
			h5dir_lev4.append(in_file[-30:-12])
		elif "Lev5" in in_file:
			vecv_5.append(v)
			vecp_5.append(p)
			test5 = 1
			h5dir_lev5.append(in_file[-30:-12])
			
		#Need to calculate average com velocity for each simulation,
		#so only include when we're at the end of one of the lists.
		if in_file != h5dir[-1]:
			name1 = in_file[-30:-16]
			idx = h5dir.index(in_file)
			name2 = h5dir[x+1]
			name2 = name2[-30:-16]
			if name1 != name2:
				#Store the values assigned to the Lev arrays in the levholder array
				levholder = []
				if test1==1:
					levholder.append(vecv_1[-1])
				if test2==1:
					levholder.append(vecv_2[-1])
				if test3==1:
					levholder.append(vecv_3[-1])
				if test4==1:
					levholder.append(vecv_4[-1])
				if test5==1:
					levholder.append(vecv_5[-1])	
				count = len(levholder)
				num = count
				#Calculate avg com velocity for the simulation depending on the number of
				#Levs it has - given from the length of the levholder array
				if num==1:
					avg = levholder
					vecv_avg.append(avg)
				elif num==2:
					spot1 = levholder[0]
					spot2 = levholder[1]
					avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2))]
					vecv_avg.append(avg)
				elif num==3:
					spot1 = levholder[0]
					spot2 = levholder[1]
					spot3 = levholder[2]
					avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)+pow(spot3[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)+pow(spot3[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2)+pow(spot3[2],2))]
					vecv_avg.append(avg)
				elif num==4:
					spot1 = levholder[0]
					spot2 = levholder[1]
					spot3 = levholder[2]
					spot4 = levholder[3]
					avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)+pow(spot3[0],2)+pow(spot4[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)+pow(spot3[1],2)+pow(spot4[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2)+pow(spot3[2],2)+pow(spot4[2],2))]
					vecv_avg.append(avg)
				elif num==5:
					spot1 = levholder[0]
					spot2 = levholder[1]
					spot3 = levholder[2]
					spot4 = levholder[3]
					spot5 = levholder[4]
					avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)+pow(spot3[0],2)+pow(spot4[0],2)+pow(spot5[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)+pow(spot3[1],2)+pow(spot4[1],2)+pow(spot5[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2)+pow(spot3[2],2)+pow(spot4[2],2)+pow(spot5[2],2))]
					vecv_avg.append(avg)
				else:
					print("Error with level numbers")
					sys.exit()

				#Need to subtract the Lev COM velocity from the avg COM velocity for the run
				#Should be the most recent contribution to both the Lev and avg vel arrays.
				if test1==1:
					avgvel = vecv_avg[-1]
					levvel = vecv_1[-1]
					if len(avgvel)!=3:
						temp = avgvel[0]
						avgvel = temp
					if len(levvel)!=3:
						temp = levvel[0]
						levvel = temp
					diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
					vdiff1.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
				if test2==1:
					avgvel = vecv_avg[-1]
					levvel = vecv_2[-1]
					if len(avgvel)!=3:
						temp = avgvel[0]
						avgvel = temp
					if len(levvel)!=3:
						temp = levvel[0]
						levvel = temp					
					diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
					vdiff2.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
				if test3==1:
					avgvel = vecv_avg[-1]
					levvel = vecv_3[-1]
					if len(avgvel)!=3:
						temp = avgvel[0]
						avgvel = temp
					if len(levvel)!=3:
						temp = levvel[0]
						levvel = temp					
					diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
					vdiff3.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
				if test4==1:
					avgvel = vecv_avg[-1]
					levvel = vecv_4[-1]
					if len(avgvel)!=3:
						temp = avgvel[0]
						avgvel = temp
					if len(levvel)!=3:
						temp = levvel[0]
						levvel = temp					
					diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
					vdiff4.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
				if test5==1:
					avgvel = vecv_avg[-1]
					levvel = vecv_5[-1]
					if len(avgvel)!=3:
						temp = avgvel[0]
						avgvel = temp
					if len(levvel)!=3:
						temp = levvel[0]
						levvel = temp										
					diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
					vdiff5.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))

				test1=test2=test3=test4=test5=0

			#Write the simulation name and avg COM velocity to file
				sim_name = h5dir[x]
				avgvel = vecv_avg[-1]
				if len(avgvel)!=3:
					temp = avgvel[0]
					avgvel = temp
				f.write(name[-30:-16]+"         "+repr(avgvel[0])+"     "+repr(avgvel[1])+"     "+repr(avgvel[2])+"\n")
					
		else:
			name1 = in_file[-30:-16]
			#Store the values assigned to the Lev arrays in the levholder array
			levholder = []
		       	if test1==1:
	       			levholder.append(vecv_1[-1])
       			if test2==1:
       				levholder.append(vecv_2[-1])
			if test3==1:
			       	levholder.append(vecv_3[-1])
			if test4==1:
			       	levholder.append(vecv_4[-1])
			if test5==1:
			       	levholder.append(vecv_5[-1])	
		       	count = len(levholder)
		       	num = count
		       	#Calculate avg com velocity for the simulation depending on the number of
		       	#Levs it has - given from the length of the levholder array
		       	if num==1:
		       		avg = levholder
	       			vecv_avg.append(avg)
	       		elif num==2:
       				spot1 = levholder[0]
	       			spot2 = levholder[1]
				avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2))]
				vecv_avg.append(avg)
			elif num==3:
			       	spot1 = levholder[0]
			       	spot2 = levholder[1]
			       	spot3 = levholder[2]
			       	avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)+pow(spot3[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)+pow(spot3[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2)+pow(spot3[2],2))]
			       	vecv_avg.append(avg)
			elif num==4:
			       	spot1 = levholder[0]
		       		spot2 = levholder[1]
				spot3 = levholder[2]
				spot4 = levholder[3]
				avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)+pow(spot3[0],2)+pow(spot4[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)+pow(spot3[1],2)+pow(spot4[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2)+pow(spot3[2],2)+pow(spot4[2],2))]
				vecv_avg.append(avg)
			elif num==5:
				spot1 = levholder[0]
				spot2 = levholder[1]
			       	spot3 = levholder[2]
				spot4 = levholder[3]
				spot5 = levholder[4]
				avg = [math.sqrt(pow(spot1[0],2)+pow(spot2[0],2)+pow(spot3[0],2)+pow(spot4[0],2)+pow(spot5[0],2)), math.sqrt(pow(spot1[1],2)+pow(spot2[1],2)+pow(spot3[1],2)+pow(spot4[1],2)+pow(spot5[1],2)), math.sqrt(pow(spot1[2],2)+pow(spot2[2],2)+pow(spot3[2],2)+pow(spot4[2],2)+pow(spot5[2],2))]
				vecv_avg.append(avg)			
			else:
				print("Error with level numbers")
				sys.exit()

			#Need to subtract the Lev COM velocity from the avg COM velocity for the run
			#Should be the most recent contribution to both the Lev and avg vel arrays.
			if test1==1:
				avgvel = vecv_avg[-1]
				levvel = vecv_1[-1]
				diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
				vdiff1.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
			if test2==1:
			       	avgvel = vecv_avg[-1]
			       	levvel = vecv_2[-1]
			       	diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
		       		vdiff2.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
			if test3==1:
			       	avgvel = vecv_avg[-1]
			       	levvel = vecv_3[-1]
		       		diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
		       		vdiff3.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
			if test4==1:
			       	avgvel = vecv_avg[-1]
		       		levvel = vecv_4[-1]
	       			diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
       				vdiff4.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
			if test5==1:
				avgvel = vecv_avg[-1]
				levvel = vecv_5[-1]
				diff = [avgvel[0] - levvel[0], avgvel[1]-levvel[1], avgvel[2]-levvel[2]]
				vdiff5.append(math.sqrt(pow(diff[0],2)+pow(diff[1],2)+pow(diff[2],2)))
		
			#Write the simulation name and avg COM velocty to file
			avgvel = vecv_avg[-1]
			if len(avgvel)!=3:
				temp = avgel[0]
				avgvel = temp
			f.write(in_file[-30:-16]+"         "+repr(avgvel[0])+"     "+repr(avgvel[1])+"     "+repr(avgvel[2])+"\n")

	f.close()
	
	#Find the magnitude of the average vectorial velocity and store it.
	#Also split the 3xN array into 1xN arrays, each holding x,y,z values
	v_avg = []
	vecx = []
	vecy = []
	vecz = []
	for in_v in vecv_avg:
		if len(in_v)==3:
			pass
		else:
						  in_v = in_v[0]

		v_avg.append(math.sqrt(pow(in_v[0],2)+pow(in_v[1],2)+pow(in_v[2],2)))
		
		if args.scatter:
			vecx.append(in_v[0])
			vecy.append(in_v[1])
			vecz.append(in_v[2])

	if args.scatter:
	#Make 2D scatter plots of the vecx,vecy,vecz data
		plt.plot(vecx,vecy,"o")
		plt.title("Avg COM velocity: x-y plane")
		plt.xlabel("x")
		plt.ylabel("y")
		plt.xlim([-0.00012,0.00012])
		plt.ylim([-0.00012, 0.00012])
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.savefig(args.filename+"_AvgVelocity_xy.pdf")
		plt.clf()
		plt.close()
		
		plt.plot(vecx,vecz, "o")
		plt.title("Avg COM velocity: x-z plane")
		plt.xlabel("x")
		plt.ylabel("z")
		plt.xlim([-0.00012,0.00012])
		plt.ylim([-0.00012,0.00012])
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.savefig(args.filename+"_AvgVelocity_xz.pdf")
		plt.clf()
		plt.close()
		
		plt.plot(vecy,vecz, "o")
		plt.title("Avg COM velocity: y-z plane")
		plt.xlabel("y")
		plt.ylabel("z")
		plt.xlim([-0.00012, 0.00012])
		plt.ylim([-0.00012, 0.00012])
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.savefig(args.filename+"_AvgVelocity_yz.pdf")
		plt.clf()
		plt.close()

	if args.histogram:
	#Find the magnitudes for each vecp_k and vecv_k
		p_1 = []
		p_2 = []
		p_3 = []
		p_4 = []
		p_5 = []
		v_1 = []
		v_2 = []
		v_3 = []
		v_4 = []
		v_5 = []

		f = open(args.filename[0]+"_LevCOMVelocityAndPosition.txt","w")
		f.write("Run name and Lev              COM Velocity              COM Position            COM Velocity deviation\n")
		f.write("\n")

		#Append values for velocity and position to their respective arrays and print values
		
		for x in range(0,len(h5dir_lev1)):
			pos = vecp_1[x]
			p_1.append(math.sqrt(pow(pos[0],2)+pow(pos[1],2)+pow(pos[2],2)))
			vel = vecv_1[x]
			v_1.append(math.sqrt(pow(vel[0],2)+pow(vel[1],2)+pow(vel[2],2)))
			f.write(h5dir_lev1[x]+"         "+repr(v_1[-1])+"         "+repr(p_1[-1])+"         "+repr(vdiff1[x])+"\n")
		for x in range(0,len(h5dir_lev2)):
			pos = vecp_2[x]
			p_2.append(math.sqrt(pow(pos[0],2)+pow(pos[1],2)+pow(pos[2],2)))
			vel = vecv_2[x]
			v_2.append(math.sqrt(pow(vel[0],2)+pow(vel[1],2)+pow(vel[2],2)))
			f.write(h5dir_lev2[x]+"         "+repr(v_2[-1])+"         "+repr(p_2[-1])+"         "+repr(vdiff2[x])+"\n")
		for x in range(0,len(h5dir_lev3)):
			pos = vecp_3[x]
			p_3.append(math.sqrt(pow(pos[0],2)+pow(pos[1],2)+pow(pos[2],2)))		
			vel = vecv_3[x]
			v_3.append(math.sqrt(pow(vel[0],2)+pow(vel[1],2)+pow(vel[2],2)))
			f.write(h5dir_lev3[x]+"         "+repr(v_3[-1])+"         "+repr(p_3[-1])+"         "+repr(vdiff3[x])+"\n")
		for x in range(0,len(h5dir_lev4)):
			pos = vecp_4[x]
			p_4.append(math.sqrt(pow(pos[0],2)+pow(pos[1],2)+pow(pos[2],2)))
			vel = vecv_4[x]
			v_4.append(math.sqrt(pow(vel[0],2)+pow(vel[1],2)+pow(vel[2],2)))
			f.write(h5dir_lev4[x]+"         "+repr(v_4[-1])+"         "+repr(p_4[-1])+"         "+repr(vdiff4[x])+"\n")
		for x in range(0,len(h5dir_lev5)):
			pos = vecp_5[x]
			p_5.append(math.sqrt(pow(pos[0],2)+pow(pos[1],2)+pow(pos[2],2)))
			vel = vecv_5[x]
			v_5.append(math.sqrt(pow(vel[0],2)+pow(vel[1],2)+pow(vel[2],2)))
			f.write(h5dir_lev5[x]+"         "+repr(v_5[-1])+"         "+repr(p_5[-1])+"         "+repr(vdiff5[x])+"\n")	
			    
#Create a logarathmic historgram with velocity info.
		plt.hist(v_avg, color = "b", bins = [0,pow(10,-8.5),1e-8,pow(10,-7.5),1e-7,pow(10,-6.5),1e-6,pow(10,-5.5),1e-5,pow(10,-4.5),1e-4,pow(10,-3.5),1e-3,pow(10,-2.5),1e-2,pow(10,-1.5),1e-1,pow(10,-0.5),1])
		plt.gca().set_xscale("log")
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.title("Frequency of avg COM velocity magnitudes")
		plt.xlabel("Order of magnitude")
		plt.ylabel("Frequency")
		plt.ylim([0,70])

	#Save histogram to file and clear plt.
		plt.savefig(args.filename+"_AvgCOMVelocity.pdf")
		plt.clf()
		plt.close()

	#Make and save histograms for each of the vecv and vecp arrays
	#Save each velocity histogram to one plot, and each position histogram to one plot
	#Commented plt.hist sections are for if one wants stacked histogram plots instead of side-by-side
		plt.hist([v_1,v_2,v_3,v_4,v_5], color = ["b","g","r","k","m"],bins = [0,pow(10,-8.5),1e-8,pow(10,-7.5),1e-7,pow(10,-6.5),1e-6,pow(10,-5.5),1e-5,pow(10,-4.5),1e-4,pow(10,-3.5),1e-3,pow(10,-2.5),1e-2,pow(10,-1.5),1e-1,pow(10,-0.5),1] ,label = ["Lev1","Lev2","Lev3","Lev4","Lev5"])	
	
		plt.gca().set_xscale("log")
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.title("Frequency of COM velocity magnitudes")
		plt.xlabel("Order of magnitude")
		plt.ylabel("Frequency")
		plt.ylim([0,50])
		
		ax = plt.subplot(111)
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
		plt.legend(loc = "center left", bbox_to_anchor = (1,0.5), prop = {'size':7})

		plt.savefig(args.filename+"_LevCOMVelocity.pdf")
		plt.clf()
		plt.close()

		
		plt.hist([p_1,p_2,p_3,p_4,p_5], color = ["b","g","r","k","m"], bins = [0,pow(10,-8.5),1e-8,pow(10,-7.5),1e-7,pow(10,-6.5),1e-6,pow(10,-5.5),1e-5,pow(10,-4.5),1e-4,pow(10,-3.5),1e-3,pow(10,-2.5),1e-2,pow(10,-1.5),1e-1,pow(10,-0.5),1] ,label = ["Lev1","Lev2","Lev3","Lev4","Lev5"])

		plt.gca().set_xscale("log")
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.title("Frequency of COM position magnitudes")
		plt.xlabel("Order of magnitude")
		plt.ylabel("Frequency")
		plt.ylim([0,50])

		ax = plt.subplot(111)
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
		plt.legend(loc = "center left", bbox_to_anchor = (1,0.5), prop = {'size':7})

		plt.savefig(args.filename+"_LevCOMPosition.pdf")
		plt.clf()
		plt.close()
		

	#Make plots for the avg com vel - lev com vel
		plt.hist([vdiff1,vdiff2,vdiff3,vdiff4,vdiff5], color = ["b","g","r","k","m"],bins = [0,pow(10,-8.5),1e-8,pow(10,-7.5),1e-7,pow(10,-6.5),1e-6,pow(10,-5.5),1e-5,pow(10,-4.5),1e-4,pow(10,-3.5),1e-3,pow(10,-2.5),1e-2,pow(10,-1.5),1e-1,pow(10,-0.5),1],label = ["Lev1","Lev2","Lev3","Lev4","Lev5"])

		plt.gca().set_xscale("log")
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.title("Frequency of COM velocity deviation from run avg COM Velocity")
		plt.xlabel("Order of magnitude")
		plt.ylabel("Frequency")
		plt.ylim([0,60])

		ax = plt.subplot(111)
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
		plt.legend(loc = "center left", bbox_to_anchor = (1,0.5), prop = {'size':7})

		plt.savefig(args.filename+"_LevCOMVelocityDeviation.pdf")
		plt.clf()
		plt.close()


if __name__ == "__main__":
	main()
