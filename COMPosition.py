#!/usr/bin/env python

"""
Calculate the COM position  drift from any run containing 
Horizons.h5 in its path. You should speicify where to 
find the runs you want to find the COM velocity drift for, 
as well as the location and name of the output files.

The program will return nothing if the directory does not 
contain a Horizons.h5 file anywhere.

The program also has the capability of making a histogram
from the positions found, plotting them logarithmically.

Specify "-h" to see proper usage.
"""

import os
import argparse
import math
import numpy as np
import matplotlib.pyplot as plt

def blockPrint():
#prevent printing to screen or to files
	sys.stdout = open(os.devnull, 'w')

def enablePrint():
#enable printing
	sys.stdout = sys.__stdout__

#------------------------------------------------------------------------------------------
	
def main():
	
	#Parse arguments from command line
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument("--dir",  nargs = "+", required = True,
			    help = "Directory in which to start searching for Horizon.h5 files.")
	parser.add_argument("--filename", required = True,
			    help = "Output file name and location. This will overwrite files that already exist with the same name.")
	parser.add_argument("--histogram", dest = "histogram",
			    help = "Include this flag to have a logarithmic histogram of the COM position magnitudes.")
	
	args = parser.parse_args()

	#Import scri module
	import scri.SpEC

	#Find and store all of the directories within the given directory that contain Horizons.h5.
	#os.path.join combines the directory name d and file name x for simplicity when calling the 
	#scri functions.
	h5dir = [os.path.join(d,x)
		 for d, dirs, files in os.walk(args.dir[0], topdown=True)
		 for x in files if x.endswith("Horizons.h5")]

	#Open and prep file for printing data.
	f = open(args.filename+".txt",'w')
	f.write('Run Name and Lev	COM position magnitude\n')
	f.write('\n')

	plist = []
	
	for in_file in h5dir:
		blockPrint() #stop estimate_avg_com_motion() from printing to screen
		
		#Store data from scri into the array cominfo. 
		#cominfo should hold items as [[x_i, x_j, x_k], [v_i, v_j, v_z], ti, tf], where
		#x and v are the COM positions and velocities.
		cominfo = scri.SpEC.estimate_avg_com_motion(in_file)
		enablePrint()
		
		#Store position information in another array to manipulate
		p = cominfo[0]

		#Calculate COM position magnitude and store in array plist
		pmag = math.sqrt(pow(p[0],2) + pow(p[1],2) + pow(p[2],2))
		plist.append(pmag)

		#Rename paths in h5dir to only include the section "PrecBBH000XXX/Lev#"
		in_file = in_file[-30:-12]
		#Print info to file
		f.write(in_file+"         "+repr(pmag))

	f.close()

	if args.histogram:			    
	#Create a logarathmic historgram with velocity info.
		plt.hist(plist, color = "b", bins = [0,pow(10,-8.5),1e-8,pow(10,-7.5),1e-7,pow(10,-6.5),1e-6,pow(10,-5.5),1e-5,pow(10,-4.5),1e-4,pow(10,-3.5),1e-3,pow(10,-2.5),1e-2,pow(10,-1.5),0.1,pow(10,-0.5),1])
		plt.gca().set_xscale("log")
		plt.grid(b=True, which="major", color = "g", linestyle = "-")
		plt.title("Frequency of COM position magnitudes")
		plt.xlabel("Order of magnitude")
		plt.ylabel("Frequency")

	#Save histogram to file and clear plt.
		plt.savefig(args.filename+"_Histogram.pdf")
		plt.clf()
		plt.close()

if __name__ == "__main__":
	main()
