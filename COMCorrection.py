#!/usr/bin/env python

"""
Generate COM corrected gw-mode data.
The user can specify more than one .h5 file 
to be considered.

Extrapolation orders Extrapolated_{N2,N3,N4}.dir 
and OutermostExtraction.dir are corrected for 
each .h5 file.

Values for beginning_fraction and
ending_fraction are t_relax and 10% respectively
by default.

"""

import os
import sys
import math
import argparse
import numpy as np
import matplotlib.pyplot as plt
import scri.SpEC

def blockPrint():
#prevent printing to screen or to files
	sys.stdout = open(os.devnull, 'w')

def enablePrint():
#enable printing
	sys.stdout = sys.__stdout__

def main():

    	#Parse Arguments from command line
	parser = argparse.ArgumentParser(description=__doc__)

	parser.add_argument("--dir", nargs = "+", required = "True",
                        help = "Directory to start looking for specified .h5 files.")
	parser.add_argument("--plot", action = "store_true",
			    help = "Generate plots of COM corrected data.")
	parser.add_argument("--beginning_fraction", type = float,
			    help = "Fraction of the total simulation to remove from the beginning when creating CoM corrected file. Default is the relazation time.")
	parser.add_argument("--ending_fraction", type = float,
			    help = "Fraction of total simulation to remove from the end of the simulation when creating CoM corrected files. Default is 10%.")
    	args = parser.parse_args()
	
	h5files = ['rhOverM_Asymptotic_GeometricUnits.h5','rMPsi4_Asymptotic_GeometricUnits.h5']

	#If beginning and ending fractions (bf,ef) have been specified, assign values to variables

	if args.beginning_fraction:
		bf = args.beginning_fraction[0]
	else:
		bf_input = -1               #Place holder for if the beginning fraction was not specified

	if args.ending_fraction:
		ef = args.ending_fraction[0]
	else:
		ef = 0.1  #assign as 10% if not specified

	#Find and store all of the directories within the given directory that contain the specified h5 directories.
	#os.path.join combines the directory name d and file name x for simplicity when calling the 
	#scri functions. Continue COM corrections within the for loop.
        #Skip directories that already contain COM corrected data with the same naming conventions used by scri

	for idx in range(0, len(h5files)):
		in_files = [os.path.join(d,x)
				 for d, dirs, files in os.walk(args.dir[0], topdown=True)
				 for x in files if (x.endswith(h5files[idx]) and not os.path.isfile(d+"/"+x[:-3]+"_CoM.h5"))]
		
		#Store the length of the h5 directory name for furture use
		filelength = len(h5files[idx])

		#Run over all files that were in the specified starting directory
		for in_file in in_files:
				
			#Remove the COM drifts from every extrapolation order for each rhOverM[].h5/rMPsi4[].h5 directory.
			#Assume that each rhOverM[].h5 directory has the N2, N3, N4, and OutermostExtraction subdirs.
	
			#Include the path, path to Horizons.h5, skip beginning fraction, skip ending fraction, whether
			#to include plots, and choosing to 'a'ppend the info to one directory to prevent overwriting.

			#If beginning fraction (bf) was not specified, get the beginning fraction value by first parsing 
			#metadata.txt for t_relax
			
			if bf_input==-1:
				metaname = in_file[:-filelength]+"metadata.txt"
				
				with open(metaname,"r") as metafile:
					for line in metafile:
						if "relaxed-measurement-time = " in line:
							#Extract the relaxed time and assign as float
							tryfloat = []
							for s in line.split():
								try:
									tryfloat.append(float(s))
								except ValueError:
									pass
							time_relaxed = tryfloat[0]

						if "common-horizon-time  = " in line:
							#Extract the time and assign as a float. 
							tryfloat = []
							for s in line.split():
							       	try:
							       		tryfloat.append(float(s))
							       	except ValueError:
							       		pass
							time_comhorizon = tryfloat[0]

				bf = time_relaxed/time_comhorizon
			       		
			blockPrint() #Prevent excessive printing to screen

			#Comstruct the COM-corrected h5 files for each extrapolation order
			#Inputs for scri are: file path, Horizons.h5 file path, beginning fraction, ending fraction, plot, writing 
			#or appending to COM corrected h5 file.
	
			scri.SpEC.remove_avg_com_motion(in_file+"/Extrapolated_N2.dir",None, bf,ef,args.plot,'a')
			scri.SpEC.remove_avg_com_motion(in_file+"/Extrapolated_N3.dir",None, bf,ef,args.plot,'a')
			scri.SpEC.remove_avg_com_motion(in_file+"/Extrapolated_N4.dir",None, bf,ef,args.plot,'a')
			scri.SpEC.remove_avg_com_motion(in_file+"/OutermostExtraction.dir",None, bf,ef,args.plot,'a')

			enablePrint() #Enable print to screen

if __name__=="__main__":
    main()
